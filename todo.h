#ifndef TODO_H
#define TODO_H

#define TODO_MAX_ACTION_NAME_LENGTH 200
#define TODO_ACTION_CLOSE_TODO 0
#define TODO_ACTION_SHOW_MENU 1
#define TODO_ACTION_NEW_TODO 2
#define TODO_ACTION_PRINT_LIST 3
#define MAX_TODO_LENGHT 100

typedef struct {
  int id;
  char title[100];         // proizvoljno sam stavila da moze ic do 100 znakova
  char description[1024];  // proizvoljno sam stavila da moze ic do 1024 znakova
  unsigned int active;
  char created_at[50];      // proizvoljno sam stavila da moze ic do 50 znakova
  unsigned int importance;  // 1 = lowest 4 =greatest
} todo;

todo create_todo();

#endif
