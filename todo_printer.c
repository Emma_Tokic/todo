#include <stdio.h>
// stdio - std = standard, i = input, o = output
#include <string.h>

#include "todo.h"
#include "todo_printer.h"

void print_help() { printf("Help me \n"); }

void print_spaces(int length) {
  for (int i = 0; i < length; i++) {
    printf(" ");
  }
}

void print_action_name(char* action_name) {
  // strlen - vrati mi broj znakova u stringu koje sam prosljedila
  size_t atction_name_length = strlen(action_name);
  // ternarnim operatorm provjeravam dali je broj znakova u varijabli
  // atction_name_length manji od 100 ako je postavljam na 100 ako nije umzimam kolika je vrijednost
  // atction_name_length
  int buffer_length = atction_name_length < 100 ? 100 : atction_name_length;
  char fill = '-';
  // Nulu sam stavila da oznacim kraj stringa
  char buffer[TODO_MAX_ACTION_NAME_LENGTH] = {0};
  // memset - u baffer varijablu mi napuni fill karaktera duzine definirane varijablom  len
  char* horizontal_border = memset(buffer, fill, buffer_length);

  printf("%s+%s+%s\n", TODO_COLOR_RED, horizontal_border, TODO_TURN_OF_ALL_ATTRIBUTES);

  printf("%s|%s", TODO_COLOR_RED, TODO_TURN_OF_ALL_ATTRIBUTES);

  int number_of_spaces = (buffer_length - atction_name_length);
  int half_number_spaces = number_of_spaces / 2;

  print_spaces(half_number_spaces);
  printf("%s%s%s", TODO_COLOR_GREEN, action_name, TODO_TURN_OF_ALL_ATTRIBUTES);

  if (number_of_spaces % 2 != 0) {
    half_number_spaces += 1;
  }

  print_spaces(half_number_spaces);
  printf("%s|%s", TODO_COLOR_RED, TODO_TURN_OF_ALL_ATTRIBUTES);

  printf("\n");

  printf("%s+%s+%s\n", TODO_COLOR_RED, horizontal_border, TODO_TURN_OF_ALL_ATTRIBUTES);
}

void print_scanf_label(char* label) {
  printf("%s%s%s:%s\n", TODO_BACKGROUND_COLOR_WHITE, TODO_COLOR_BLACK, label,
         TODO_TURN_OF_ALL_ATTRIBUTES);
}
