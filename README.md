# Todo project

```bash
# Build program
gcc -g -Wall -o ema main.c


# Git

# check git versio
git --version

# Init git project
git init

#add remote address of my new repository. Example
git remote add origin https://Emma_Tokic@bitbucket.org/Emma_Tokic/imenik.git
git push -u origin master

# chack files state
git status

# use only if file added for tracking
git add -p
git add "putanja do datoteke sa njenim imenom i ekstezijama"

# add file for tracking
git add file-path

# commit changes with message
git commit -m "this is my message"

# how to push commited code to remote repository
git push

# how to list commit repository
git log
git log --oneline

# Set my profile
git config --global user.email "emmatokic100@gmail.com"
git config --global user.name "Emma"
```