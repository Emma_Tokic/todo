#include <stdio.h>
#include <string.h>

#include "todo.h"
#include "todo_printer.h"

int main(int argc, char** argv) {
  int run_todo = 1;
  int action = -1;
  todo list[MAX_TODO_LENGHT];
  int current_list_index = 0;

  print_action_name("Hello type one for help");

  while (run_todo) {
    //@TODO dodaj labelu koju novu akciju terba izvrsit
    scanf("%d", &action);

    switch (action) {
      case TODO_ACTION_SHOW_MENU:
        print_action_name("To do maneu");
        break;

      case TODO_ACTION_NEW_TODO:
        print_action_name("create new todo");

        list[current_list_index] = create_todo();
        current_list_index++;

        break;
      case TODO_ACTION_PRINT_LIST:
        for (int i = 0; i < current_list_index; i++) {
          printf("id %d \n", list[i].id);
          printf("title %s \n", list[i].title);
          printf("description %s \n", list[i].description);
          printf("active %d \n", list[i].active);
          printf("created at %s \n", list[i].created_at);
          printf("importance %d \n", list[i].importance);
          printf("------- \n");
        }
        break;

      case TODO_ACTION_CLOSE_TODO:
        print_action_name("Close todo");
        run_todo = 0;
        break;

      default:
        // Izvuci sve znakove iz standarnog  inputa tako da scanf moze zaustavilti loop petlju da se
        // ne desi infinitiva petlja jer smo gore definirali itiger varijablu a mozemo upisati slova
        // i scanf ne moze naprviti konverziju tog teksta u intiger posljedica toga ostaje \n u
        // inputu i vrti petlju beskonacno
        while (getchar() != '\n')
          ;

        print_action_name("Whats wrong with you");
        break;
    }
  }

  print_action_name("Shutting down todo");

  return 0;
}