#ifndef TODO_PRINTER_H
#define TODO_PRINTER_H

// Everything you never wanted to know about ANSI escape codes
// https://notes.burke.libbey.me/ansi-escape-codes/
//
// \x1b
// ANSI escapes always start with \x1b, or \e, or \033.
// These are all the same thing: they’re just various ways of
// inserting the byte 27 into a string. If you look at an ASCII
// table, 0x1b is literally called ESC, and this is basically why.

#define TODO_COLOR_BLACK "\x1B[30m"
#define TODO_COLOR_RED "\x1B[31m"
#define TODO_COLOR_GREEN "\x1B[32m"
#define TODO_COLOR_YELLOW "\x1B[33m"
#define TODO_COLOR_CYAN "\x1B[36m"
#define TODO_COLOR_WHITE "\x1B[37m"

#define TODO_BACKGROUND_COLOR_BLACK "\x1B[40m"
#define TODO_BACKGROUND_COLOR_RED "\x1B[41m"
#define TODO_BACKGROUND_COLOR_GREEN "\x1B[42m"
#define TODO_BACKGROUND_COLOR_YELLOW "\x1B[43m"
#define TODO_BACKGROUND_COLOR_CYAN "\x1B[46m"
#define TODO_BACKGROUND_COLOR_WHITE "\x1B[47m"

#define TODO_FONT_STYLE_BOLD "\x1B[1m"
#define TODO_FONT_STYLE_ITALIC "\x1B[3m"

#define TODO_TURN_OF_ALL_ATTRIBUTES "\x1B[0m"

void print_help();
void print_action_name(char* action_name);
void print_scanf_label(char* label);
char* get_string_with_spaces(int length);

#endif