CC=gcc
CFLAGS=-g -Wall

#recepi
#target:dependecies
#action

todo: main.o todo_printer.o todo.o
	$(CC) main.o todo_printer.o todo.o -o todo.exe

main.o: main.c
	$(CC) $(CFLGS) -c main.c

todo_printer.o: todo_printer.c
	$(CC) $(CFLGS) -c todo_printer.c

todo.o: todo.c
	$(CC) $(CFLGS) -c todo.c


clear:
	$(RM) *.exe *.o