#include "todo.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "todo_printer.h"

todo create_todo() {
  todo t;

  t.id = rand();

  print_scanf_label("add todo title");
  scanf("%s", t.title);
  getchar();  // Moram izvuci new line \n znak tako da sljedeci scanf može normalno fukcjonirati

  print_scanf_label("add todo description");
  scanf("%s", t.description);
  getchar();  // Moram izvuci new line \n znak tako da sljedeci scanf može normalno fukcjonirati

  char active;
  t.active = 1;

  print_scanf_label("would you like to seet this todo as active: Y/n");
  scanf("%c", &active);
  getchar();  // Moram izvuci new line \n znak tako da sljedeci scanf može normalno fukcjonirati

  if (active != 'Y' || active != 'y') {
    t.active = 0;
  }

  print_scanf_label("please enter created at timestam (eg. 22.3.1994.)");
  scanf("%s", t.created_at);
  getchar();  // Moram izvuci new line \n znak tako da sljedeci scanf može normalno fukcjonirati

  print_scanf_label(
      "how important this todo is to you. type number: low = 1, high = 2, higher = 3, the higest = "
      "4");
  scanf("%d", &t.importance);
  getchar();  // Moram izvuci new line \n znak tako da sljedeci scanf može normalno fukcjonirati
              // 123\n

  return t;
};
